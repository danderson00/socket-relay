const createConnections = require('./connections')
const connectApi = require('./connectApi')

module.exports = options => ({ log, api: { add: addApi } }) => {
  const connections = createConnections(options, addApi, log)

  return {
    name: 'relay',
    api: { connectApi: () => {}, heartbeat: () => {} },
    middleware: { connectApi: connectApi(options, connections) }
  }
}