const consumer = require('@x/socket/consumer')

module.exports = (options, addApi, log) => {
  const connections = {}

  const futureValue = () => {
    let set
    const promise = new Promise(resolve => {
      set = value => resolve(value)
    })
    promise.set = set
    return promise
  }

  const createSocketFactory = socket => {
    let promise
    const factory = () => socket.readyState === 1 ? socket : (promise = futureValue())
    factory.register = socket => promise.set(socket)
    return factory
  }

  const create = async socket => {
    const socketFactory = createSocketFactory(socket)

    await consumer({ socketFactory })
      .connect()
      .then(api => addApi(api))
      .catch(error => log.error('Error connecting to host API', error))

    return socketFactory.register
  }

  const register = async (name, socket) => {
    if(connections[name]) {
      connections[name](socket)
    } else {
      connections[name] = await create(socket)
    }
  }

  return { register }
}