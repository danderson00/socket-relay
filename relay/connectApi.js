module.exports = (options = {}, connections) => {
  const authorize = async user => !options.authorize || (
    typeof options.authorize === 'function'
      ? options.authorize(user)
      : Object.keys(options.authorize).reduce(
          (result, field) => result && user && user[field] === options.authorize[field],
          true
        )
  )
  return async ({ connection }, name) => {
    if(!await authorize(connection.user)) {
      throw new Error('Not authorized')
    }

    await connections.register(name, connection.socket)
  }
}
