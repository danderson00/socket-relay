const observable = require('@x/observable')
const { setup, delay } = require('./setup')

test("static values", setup(
  { hello: message => 'Hello ' + message },
  async ({ api }) => {
    const result = await api.hello('world')
    expect(result).toBe('Hello world')
  }
))

test("observables", setup(
  {
    hello: message => observable(
      publish => setTimeout(() => publish(message), 20),
      { initialValue: 'Initial' }
    )
  },
  async ({ api }) => {
    const result = await api.hello('world')
    expect(result()).toBe('Initial')
    await delay()
    expect(result()).toBe('world')
  }
))

const o = observable.subject({ initialValue: 'Initial' })
test("observables survive disconnection", setup(
  {
    getObservable: () => o,
    setValue: message => o.publish(message)
  },
  async ({ api, recreateHost }) => {
    const result = await api.getObservable()
    expect(result()).toBe('Initial')
    await api.setValue('1')
    expect(result()).toBe('1')
    await recreateHost()
    expect(result()).toBe('1')
    await api.setValue('2')
    expect(result()).toBe('2')
    await recreateHost()
    expect(result()).toBe('2')
    await api.setValue('3')
    expect(result()).toBe('3')
  }
))

test("multiple named connections", setup(
  { hello1: () => '1' },
  async ({ api, createNewHost, connect }) => {
    expect(await api.hello1()).toBe('1')
    await createNewHost({ hello2: () => '2' }, { name: 'two' }, 1236)
    const api2 = await connect()
    expect(await api2.hello1()).toBe('1')
    expect(await api2.hello2()).toBe('2')
  },
  { name: 'one' }
))
