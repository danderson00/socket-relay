const WebSocket = require('ws')
const hostModule = require('@x/socket/host')
const relayFeature = require('../relay')
const hostFeature = require('../host')
const authFeature = require('@x/socket.auth/host')
const consumerModule = require('@x/socket/consumer')

const delay = (ms = 50) => new Promise(resolve => setTimeout(resolve, ms))

const createHost = port => {
  const server = new WebSocket.Server({ port })
  const host = hostModule({ server, log: { level: 'fatal' } })
  const close = () => new Promise(resolve => server.close(resolve))
  return { host, close }
}

const setup = (api, test, options = {}) => async () => {
  const relay = createHost(1235)
  relay.host
    .useFeature(authFeature({
      external: ({ code }) => code === '1234' ? { username: 'user' } : undefined
    }))
    .useFeature(relayFeature(options.relay))

  const createApiHost = (api, options, port = 1234) => {
    const host = createHost(port)
    host.host.useApi(api)
    host.host.useFeature(hostFeature({ relayUrl: 'ws://localhost:1235', ...options }))
    return host
  }

  let host = createApiHost(api, options.host)
  const additionalHosts = []

  const recreateHost = async () => {
    await host.close()
    host = createApiHost()
  }

  const createNewHost = async (api, options, port) => {
    const newHost = createApiHost(api, options, port)
    additionalHosts.push(newHost)
    await delay()
  }

  const connect = () => consumerModule({ socketFactory: () => new WebSocket('ws://localhost:1235') }).connect()

  await delay()

  try {
    const api = await connect()
    await test({ api, recreateHost, createNewHost, connect })
  } finally {
    await host.close()
    await relay.close()
    await Promise.all(additionalHosts.map(({ close }) => close()))
  }
}

module.exports = { setup, delay }