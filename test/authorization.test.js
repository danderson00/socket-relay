const { setup } = require('./setup')

test("authorization with static values", setup(
  { hello: message => 'Hello ' + message },
  async ({ api }) => {
    expect(await api.hello('world')).toBe('Hello world')
  },
  {
    relay: { authorize: { username: 'user' } },
    host: { authentication: { provider: 'external', code: '1234' } }
  }
))

test("authorization with function", setup(
  { hello: message => 'Hello ' + message },
  async ({ api }) => {
    expect(await api.hello('world')).toBe('Hello world')
  },
  {
    relay: { authorize: user => user && user.username === 'user' },
    host: { authentication: { provider: 'external', code: '1234' } }
  }
))

test("authentication with function", setup(
  { hello: message => 'Hello ' + message },
  async ({ api }) => {
    expect(await api.hello('world')).toBe('Hello world')
  },
  {
    relay: { authorize: { username: 'user' } },
    host: { authentication: () => ({ provider: 'external', code: '1234' }) }
  }
))