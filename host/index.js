const createConsumer = require('@x/socket/consumer')
const Websocket = require('ws')

module.exports = options => ({ log, connections }) => {
  log = log.child('socket.relay.host')
  const { relayUrl, authentication, name, apiKey } = options

  const socketFactory = () => {
    const socket = new Websocket(relayUrl)
    connections.add(socket)
    return socket
  }

  const getAuthentication = async () => authentication &&
    typeof authentication === 'function' ? authentication() : authentication

  const consumer = createConsumer({ socketFactory })
    .useFeature('reestablishSessions')
    .useFeature('heartbeat')

  if(apiKey) {
    consumer.useFeature('apiKey', apiKey)
  }

  consumer.connect()
    .then(async ({ authenticate, connectApi }) => {
      const payload = await getAuthentication()
      if(payload) {
        if(authenticate) {
          const { success } = await authenticate(payload)
          if(!success) {
            throw new Error('Authentication failure')
          }
        } else {
          log.warn('Authentication payload provided but authentication not enabled on relay')
        }
      }
      await connectApi(name)
    })
    .catch(error => log.error('Error connecting to relay', error))

  return {
    name: 'relay'
  }
}